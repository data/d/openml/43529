# OpenML dataset: Chess-Games-of-Woman-Grandmasters-(2009---2021)

https://www.openml.org/d/43529

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Chess
A lot has changed in chess over the years with computer engines and AI algorithms. But how has human performance changed along with the rise in technology?
WGM
Becoming a Woman Grandmaster (WGM) is no easy feat. Its the highest-ranking chess title to woman and over the lifetime of FIDE, as of January, 2020 there are only 458 WGMs in the world. Certainly a very elite, strong and accomplished set of individuals.
Content
The dataset consists of all games played by Woman Grandmasters on chess.com from September-2009. The game moves are available in the standard PGN format along with metadata information of the game, players, results and ratings.
The notebook for preparing this dataset is published here.
Acknowledgements
chess.com for making these games publicly available.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43529) of an [OpenML dataset](https://www.openml.org/d/43529). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43529/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43529/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43529/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

